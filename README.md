# Festival "caf conc'"


## Application de gestion de groupes de musique

# Contexte

Le festival `Caf conc'`, organisant des café concerts au sein de ville le temps d'une semaine, a des soucis dernièrement avec son logiciel pour la gestion des concerts et a fait appel à vous. Ils aimeraient que vous fassiez un prototype pour un outil le remplaçant.


Une analyse a été réalisée par votre chef de projet et il a morcelé le tout en plusieurs sprints.

Dans un premier temps, l'application devra juste pouvoir permettre la saisie des groupes.
Et il vous demande d'en réaliser la modélisation de la base de données.

Chaque groupe devra s'inscrire et saisir l'ensemble des musiciens qui le composent.

Un groupe se distinguera par un nom, une catégorie de musique, un pays d'origine, un email de contact, une ou plusieurs photos, une description, des instruments qui le composent et d'un tarif de prestation en euros.
Un musicien peut appartenir à plusieurs groupes.
Chaque musicien aura également sa propre fiche avec son nom et prénom, un descriptif ainsi qu'une liste des instruments qu'il sait jouer.
Attention, certains groupes peuvent jouer des instruments, même s'il n'ont pas de musiciens!

Un musicien pourra jouer d'un instrument dans un groupe et ne pas jouer avec l'instrument dans l'autre groupe. 

Afin de faciliter la logistique, il est important de pouvoir ajouter une description sur l'association entre les instruments et les musiciens ou les groupes. 
Ils pourront ainsi donner des informations complémentaires quant aux particularités de l'instrument.


Le second sprint est déjà en train d'être planifié afin de réaliser la gestion des lieux et du planning des concerts.


### Production attendue

- Livrez le dictionnaire de données
- Livrez le Modèle Conceptuel de Données
- Livrez le Modèle Physique de Données
- Livrez un fichier SQL modélisant la base

 
## Sprint 2

### Production attendue
 - Livrez un fichier SQL permettant de faire migrer la base de données préalable


Pour la gestion des lieux, il faut savoir que le festival est itinérant et se déplace de bar en bar. 
Ainsi il faut pouvoir répertorier un lieu (Ce dernier aura un nom, une adresse, un description, un numéro de téléphone et email de contact et une coordonée gps et éventuellement une image)
Et sera rattachée à une commune. 
La commune quant à elle aura un nom et un code postal.


Modifications du sprint précédent: 
    - Suite à votre première livraison, le client a également demandé à ce qu'il y ait une description et un logo pour les catégories musicales des groupes de musique. 

Cette fois-ci le client vous demande également de lui fournir un jeu de données.  Il vous fournit cette liste-ci

Communes : 
 - Rennes, 35000
 - Paris, 75000
 - Toulouse, 31000

Bar : 
 - nom: La Guinguette du MeM
 - adresse: La Piverdière, Rte de Sainte-Foix, 35000 Rennes
 - desc: Palettes et boules à facettes confèrent une ambiance bohème à cette salle de concert au bord de l'eau dotée d'un bar en plein air.
 - email: lemem@mailo.org
 - tel : +33299456789
 - coord: 48.1031338,-1.7194539

Bar : 
 - nom: La Banque Pub Cocktail
 - adresse: 
 - desc: Pub cosy historique ouvert tard le soir avec cocktails créatifs, diffusion d'événements sportifs et terrasse.
 - email: banque.pub@finghy.com
 - tel: +33977051219
 - coord: 48.1126832,-1.6837181


Groupes :
 - {nom: "Daft Punk", categ: "Electro",  desc: ""}


Le troisième sprint est déjà en train d'être planifié afin de réaliser le planning des concerts.


<!--
 -->

